require('dotenv').config()


const express = require('express');
const bodyParser = require("body-parser");
const basicAuth = require('express-basic-auth');
const { body, validationResult, check } = require('express-validator');

const { query, aggregate, last_query, sum_aggregate } = require('./lib/mongodb.js');

const helmet = require("helmet");



const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(basicAuth({
  users: JSON.parse(process.env.USER_AUTH)
}));

app.use(helmet());

const port = process.env.PORT;

const allowed_collections = ["equipment", "land_data", "bids", "materials", "sales", "buildings"];

const { database } = require('./lib/mongo-config.js');

const { error_log_entry } = require('./lib/errors.js');

let db;

app.post('/query', body("collection").isIn(allowed_collections), async function (req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      
        const error_hanlder = new error_log_entry(req.body.collection, "query validation error", errors.errors[0])
        res.json( error_hanlder.log_handle_error() );
      
    } else {

      var collection = req.body.collection;
      var query_search = req.body.query;
      var sort = req.body.sort;
      
      
      
      var result = await query(db, collection, query_search, sort);
      res.json(result)
    }
  } catch (error) {
    next(error)
  }

});

app.post('/current_state', body("collection").isIn(allowed_collections), async function (req, res, next) {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {

        const error_hanlder = new error_log_entry(req.body.collection, "current_state validation error", errors.errors[0])
        res.json( error_hanlder.log_handle_error() );
      
    } else {

      var collection = req.body.collection;
      var query_search = req.body.query;
      var sort_params = { timestamp: -1 };


      var result = await last_query(db, collection, query_search, sort_params, 1);


      res.json(result)
    }
  } catch (error) {
    next(error)
  }

});

app.post('/insert_single', body("collection").isIn(allowed_collections), async function (req, res, next) {

  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {

      const error_hanlder = new error_log_entry(req.body.collection, "insert single validation error", errors.errors[0])
      res.json( error_hanlder.log_handle_error() );
      
    } else {

      var collection = req.body.collection;
      var entry_doc = req.body.entry_doc;
      
      entry_doc.timestamp = Date.now()

      var result = await insert(db, collection, entry_doc);
      res.json(result)
    }
  } catch (error) {
    next(error)
  }

});


app.post('/current_state_many', body("collection").isIn(allowed_collections), async function (req, res, next) {

  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {

      const error_hanlder = new error_log_entry(req.body.collection, "current_state_many validation error", errors.errors[0])
      res.json( error_hanlder.log_handle_error() );
      
    } else {

      var collection = req.body.collection;
      var filter = req.body.filter;
      var grouper = req.body.grouper;
      
      
      var result = await aggregate(db, collection, filter, grouper);
      res.json(result)
    }
  } catch (error) {
    next(error)
  }

});


app.post('/sum_aggregate', body("collection").isIn(allowed_collections), async function (req, res, next) {

  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {

      const error_hanlder = new error_log_entry(req.body.collection, "sum_aggregate error", errors.errors[0])
      res.json( error_hanlder.log_handle_error() );
      
      
    } else {

      var collection = req.body.collection;
      var filter = req.body.filter;

      var result = await sum_aggregate(db, collection, filter);
      res.json(result)
    }
  } catch (error) {
    next(error)
  }

});


// Root-level error handler
app.use(async (err, req, res, _next) => {
  console.error(`${err.toString()}\n${err.stack}`);
  // Error details can be filtered here e.g. for production environment to avoid exposing sensitive details
  res.status(500).send(err);
});



async function run() {
  try {
    db = await database();
    console.log('Connected to the database')
  } catch (err) {
    console.error('Failed to connect to the database', err);
    throw new Error('Failed to connect to the database');
  }

  app.listen(port, async () => {
    console.log(`${process.env.NAME} listening at http://localhost:${port}`);
  })
}

run();