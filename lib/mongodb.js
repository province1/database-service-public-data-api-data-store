const { error_log_entry } = require('./errors.js');


async function last_query(database, collection_name, query, sort_params, limit) {

    try {
        const collection = database.collection(collection_name);
        var result = await collection.find(query).sort(sort_params).limit(limit).toArray();
        return { error: false, result };

    } catch (e) {
        const error_hanlder = new error_log_entry(collection_name, "last_query", e.errmsg)
        return error_hanlder.log_handle_error()
    }


}

async function query(database, collection_name, query, sort_params, limit){
    
    try {
        const collection = database.collection(collection_name);
        limit = limit ?? 0;
        var result = await collection.find(query).sort(sort_params).limit(Number(limit)).toArray()
        return {error: false, result}
        
    } catch (e) {

        const error_hanlder = new error_log_entry(collection_name, "query", e.errmsg)
        return error_hanlder.log_handle_error()

    }

}



async function aggregate(database, collection_name, filter, group) {

    // ToDo: Fix the result + error handling

    var result_clean = {}

    try {
        const collection = database.collection(collection_name);

        var pipeline = [

            { $match: filter },
            { $sort: { timestamp: -1 } },
            {
                $group: {
                    _id: "$" + group,
                    latest: { $first: "$$ROOT" }
                }
            },
            
            {
                
                $replaceWith: "$latest"
                
            }
        ];

        var result = await collection.aggregate(pipeline).toArray();

        result_clean = result.map(x => {
            return x.latest
        })
        return {error: false, result};


    } catch (e) {
        const error_hanlder = new error_log_entry(collection_name, "aggregate", e.errmsg, filter, group)
        return error_hanlder.log_handle_error()

    }


}

async function sum_aggregate(database, collection_name, filter) {

    try {
        const collection = database.collection(collection_name);

        var pipeline = [

            { $match: filter },
            {
                $group: {
                    _id: "null",
                    total: { $sum: "$amount" }
                }
            },
        ];

        var result = await collection.aggregate(pipeline).toArray();
        return {error: false, result}

    } catch (e) {
        const error_hanlder = new error_log_entry(collection_name, "sum_aggregate", e.errmsg)
        return error_hanlder.log_handle_error()
    }


}


module.exports = { last_query, query, aggregate, sum_aggregate }

