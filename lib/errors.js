
class error_log_entry {
    
    constructor(collection, operation, error_msg, filter=null, group=null) {
        
        this.timestamp = Date.now();
        this.collection = collection;
        this.operation = operation;
        this.error_msg = error_msg;
        this.filter = filter;
        this.group = group;
        
    }
    
    log_handle_error(){
        
        var error_data = {  timestamp: this.timestamp,
                            collection: this.collection,
                            operation: this.operation,
                            error_msg: this.error_msg,
                            filter: this.filter,
                            group: this.group
                            
        };
        console.log("!Error: ", error_data);
        return { error: true, error_msg: this.error_msg }
        
    }
    
    
}


module.exports = { error_log_entry }