const { MongoClient, ServerApiVersion } = require("mongodb");

const uri = process.env.MDB_URL;
const client = new MongoClient(uri, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  serverApi: ServerApiVersion.v1,
  minPoolSize: 8
});



async function database(){
  
  await client.connect();
  
  return client.db();
  
  
}


module.exports = { database };

